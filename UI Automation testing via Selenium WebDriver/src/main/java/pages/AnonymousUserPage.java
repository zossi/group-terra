package pages;

import org.openqa.selenium.WebDriver;
import utils.Utils;


public class AnonymousUserPage extends BasePage{
    private static final int ONE_LOREM_WORD = 1;
    private final String EXPECTEDMESSAGEFORSEARCH = "There are no users existing in this search criteria.";
    private final String EXPECTEDMESSAGEFORLOGIN ="Wrong username or password.";
    private final String EXPECTEDALERTMESSAGE ="Please fill out this field.";


    public AnonymousUserPage(WebDriver driver) {
            super(driver, "base.url");
        }

        public void enterSearchTermByProfession(String term){
            actions.typeValueInField(term, "homePage.searchInputFieldProfession");
        }
        public void enterSearchTermByName(String term){
        actions.typeValueInField(term, "homePage.searchInputFieldName");
    }

        public void searchAndSubmitByProfession(String term1){
        enterSearchTermByProfession(actions.getTextOfElement("homePage.resultOfProfessionSearch"));
        actions.clickElement("homePage.searchButton");

        }

        public void searchAndSubmitByProfessionAndName(String term1,String term2){
            enterSearchTermByProfession(term1);
            enterSearchTermByName(term2);
            actions.clickElement("homePage.searchButton");
        }

        public void searchAndSubmitByNoneFillInFields(){
            actions.waitForInvisibilityOfElementLocated("navBarCover");
              actions.clickElement("homePage.searchButton");
        }

        public void navigateToAboutUsPage(){
            actions.clickElement("homePage.aboutUsButton");
        }

        public void loginWhenUserIsUnregistered(){
            String username = actions.getLorem().getWords(ONE_LOREM_WORD);
            String password =actions.getLorem().getWords(ONE_LOREM_WORD)+actions.getLorem().getZipCode();
            actions.clickElement("homePage.signInButton");
            actions.typeValueInField(username, "loginPage.usernameField");
            actions.typeValueInField(password, "loginPage.passwordField");
            actions.scrollDown();
            actions.clickElement("loginPage.logInButton");
        }
        public void assertResultOfSearchByProfession() {
            actions.assertElementPresent("homePage.seeProfileButton");
            actions.assertElementPresent("homePage.resultOfProfessionSearch");
        }

        public void assertMessageNotFoundByCriteria() {
        actions.assertMessagePresent(EXPECTEDMESSAGEFORSEARCH,"homePage.errorMessageForSearch");
        }

        public void assertLoginMessageNotRegisteredUser() {

        actions.assertMessagePresent(EXPECTEDMESSAGEFORLOGIN,"loginPage.errorMessage");
        }
        public void assertPopUpAlertMessageForSearchFields(){
          actions.assertPopUpAlertMessage(EXPECTEDALERTMESSAGE);
        }
    }




