package pages;

import org.openqa.selenium.WebDriver;

public class PostPage extends BasePage {
    private static final int MIN_WORDS_TO_TYPE = 5;
    private static final int MAX_WORDS_TO_TYPE = 10;

    public PostPage(WebDriver driver) {
        super(driver, "base.url");
    }


    public void createPrivatePostWithLongMessage() {
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.clickElement("postPage.newPostButton2");
        actions.scrollDown();
        actions.typeValueInField(String.valueOf(false), "postPage.selectPrivacyMenu");
        actions.typeValueInField(actions.getLorem().getWords(50, 100), "postPage.textOfPost");
        actions.clickElement("postPage.savePostButton");
    }

    public void createPublicPostWithShortMessage() {
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.clickElement("postPage.newPostButton2");
        actions.scrollDown();
        actions.selectFromDropDownMenu("true","postPage.selectPrivacyMenu" );
        actions.typeValueInField(actions.getLorem().getWords(MIN_WORDS_TO_TYPE, MAX_WORDS_TO_TYPE), "postPage.textOfPost");
        actions.clickElement("postPage.savePostButton");
    }

    public void loadAllPosts() {
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.clickElement("homePage.latestPostButton");
    }

    public void likePost() {
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.scrollDownJS();
        actions.waitForElementPresent("postPage.likeButton");
        actions.clickElement("postPage.likeButton");
    }

    public void deletePost() {
            actions.waitForInvisibilityOfElementLocated("fullscreenCover");
            actions.waitForElementPresent("postPage.exploreThisPostButton");
            actions.scrollDown();
            actions.waitForInvisibilityOfElementLocated("fullscreenCover");
            actions.waitFor(5);
            actions.waitForInvisibilityOfElementLocated("navBarCollapseCover");
            actions.clickElement("postPage.exploreThisPostButton");
            actions.waitForElementPresent("postPage.deletePostButton");
            actions.clickElementJS("postPage.deletePostButton");
            actions.waitForElementPresent("postPage.deletePostTitle");
            actions.scrollDown();
            actions.selectFromDropDownMenu("true", "postPage.dropDownMenuToDelete");
            actions.clickElement("postPage.submitDeleteBtn");
            actions.waitForInvisibilityOfElementLocated("fullscreenCover");
            actions.clickElement("homePage.homeButton");
        }
}
