package pages;

import org.openqa.selenium.WebDriver;
import utils.Utils;


public class UpdateUserProfilePage extends BasePage{
    private static final int MIN_WORDS_TO_TYPE_FOR_BIO = 2;
    private static final int MAX_WORDS_TO_TYPE_FOR_BIO = 15;
    private static final int START_VALUE_TO_SELECT_CITY = 1;
    private static final int END_VALUE_TO_SELECT_CITY = 39;
    private static final int MIN_HOUR_TO_SET_AVAILABILITY = 1;
    private static final int MAX_HOURS_TO_SET_AVAILABILITY = 164;
    private static final int MIN_WORDS_TO_TYPE_FOR_SKILL = 1;
    private static final int MAX_WORDS_TO_TYPE_FOR_SKILL = 3;
    private static final int START_VALUE_TO_SELECT_PROFESSION = 100;
    private static final int END_VALUE_TO_SELECT_PROFESSION = 157;
    public String fullNameBefore;
    public String fullNameAfter;
    public String professionBefore;

    public String professionAfter;
    public String hoursAvailabilityAfter;
    public String hoursAvailabilityBefore;

    public UpdateUserProfilePage(WebDriver driver) {
          super(driver, "base.url");
    }


    public void navigateToPersonalProfilePage(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.clickElement("updateProfilePage.personalProfileButton");
        actions.scrollDown();
        actions.waitForElementPresent("updateProfilePage.editProfileButton");
        actions.scrollDownJS();
        fullNameBefore=actions.getTextOfElement("updateProfilePage.resultUpdatedFullName");
        professionBefore=actions.getTextOfElement("updateProfilePage.profession");
        if (actions.isDisplayed("updateProfilePage.resultUpdateAvailability")){
            hoursAvailabilityBefore=actions.getTextOfElement("updateProfilePage.resultUpdateAvailability");
            actions.scrollUpJS();
            actions.clickElement("updateProfilePage.editProfileButton");
        } else{
        actions.scrollUpJS();
        actions.clickElement("updateProfilePage.editProfileButton");}
    }

    public void updateUserPersonalInfo() {

        actions.scrollDown();
        actions.waitForElementPresent("updateProfilePage.updateMyProfileButton");
        actions.clearTextField("updateProfilePage.editFirstName");
        actions.typeValueInField(actions.getLorem().getFirstName(), "updateProfilePage.editFirstName");
        actions.clearTextField("updateProfilePage.editLastName");
        actions.typeValueInField(actions.getLorem().getLastName(), "updateProfilePage.editLastName");
        actions.typeValueInField(Utils.getConfigPropertyByKey("credentials.defaultBirthday"), "updateProfilePage.editBirthday");
        actions.scrollDown();
        actions.clearTextField("updateProfilePage.editBio");
        actions.typeValueInField(actions.getLorem().getWords(MIN_WORDS_TO_TYPE_FOR_BIO, MAX_WORDS_TO_TYPE_FOR_BIO), "updateProfilePage.editBio");
        actions.selectFromDropDownMenu(Integer.toString(actions.getRandomNumber(START_VALUE_TO_SELECT_CITY, END_VALUE_TO_SELECT_CITY)),"updateProfilePage.editCity");
        actions.clickElement("updateProfilePage.updateMyProfileButton");
        actions.clickElement("updateProfilePage.profileButton");
        fullNameAfter=actions.getTextOfElement("updateProfilePage.resultUpdatedFullName");
    }

    public void updateSkillsAndHours() {

        String randomHours = actions.getRandomNumber(MIN_HOUR_TO_SET_AVAILABILITY, MAX_HOURS_TO_SET_AVAILABILITY)+".00";

        actions.clickElement("updateProfilePage.whatServiceCanYouOfferButton");
        actions.waitForElementPresent("updateProfilePage.updateButton");

        actions.clearTextField("updateProfilePage.editSkill1");
        actions.typeValueInField(actions.getLorem().getWords(MIN_WORDS_TO_TYPE_FOR_SKILL, MAX_WORDS_TO_TYPE_FOR_SKILL), "updateProfilePage.editSkill1");
        actions.clearTextField("updateProfilePage.editSkill2");
        actions.typeValueInField(actions.getLorem().getWords(MIN_WORDS_TO_TYPE_FOR_SKILL,MAX_WORDS_TO_TYPE_FOR_SKILL), "updateProfilePage.editSkill2");
        actions.clearTextField("updateProfilePage.editAvailableHour");
        actions.typeValueInField(randomHours, "updateProfilePage.editAvailableHour");
        actions.clickElement("updateProfilePage.updateButton");
        actions.scrollDown();
        hoursAvailabilityAfter= actions.getTextOfElement("updateProfilePage.resultUpdateAvailability");
    }

    public void updateProfessionalCategory (){
        actions.clickElement("updateProfilePage.editProfessionalButton");
        actions.waitForElementPresent("updateProfilePage.updateProfessionButton");
        actions.selectFromDropDownMenu(Integer.toString(actions.getRandomNumber(START_VALUE_TO_SELECT_PROFESSION,END_VALUE_TO_SELECT_PROFESSION)),"updateProfilePage.updateUserProfession");
        actions.clickElement("updateProfilePage.updateProfessionButton");
        actions.scrollDownJS();
        professionAfter=actions.getTextOfElement("updateProfilePage.profession");
    }

}
