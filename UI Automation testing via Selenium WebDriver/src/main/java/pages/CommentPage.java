package pages;

import org.openqa.selenium.WebDriver;

public class CommentPage extends BasePage{
    private static final int MIN_WORDS_TO_TYPE = 5;
    private static final int MAX_WORDS_TO_TYPE = 20;

    public CommentPage(WebDriver driver) {
        super(driver, "base.url");
    }

     public void loadAllPosts(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.waitForElementPresent("commentPage.latestPostsButton");
        actions.moveToElement("commentPage.latestPostsButton");
        actions.clickElement("commentPage.latestPostsButton");
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.waitForElementPresent("commentPage.browseAllPublicPostButton");
        actions.moveToElement("commentPage.browseAllPublicPostButton");
        actions.clickElement("commentPage.browseAllPublicPostButton");

    }
    public void navigateToUserProfile(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.scrollDown();
        actions.waitForElementPresent("commentPage.exploreUserPostBtn");
        actions.moveToElement("commentPage.exploreUserPostBtn");
        actions.clickElement("commentPage.exploreUserPostBtn");
    }

    public void createComment(){
        actions.scrollDown();
        actions.typeValueInField(actions.getLorem().getWords(MIN_WORDS_TO_TYPE, MAX_WORDS_TO_TYPE), "commentPage.textFieldForComment");
        actions.clickElement("commentPage.postCommentBtn");

    }
    public void likeComment(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.waitForElementPresent("commentPage.showCommentsBtn");
        actions.scrollUntilElementIsDisplayed("commentPage.showCommentsBtn");
        actions.clickElement("commentPage.showCommentsBtn");
        actions.waitForElementPresent("commentPage.likeButton");
        actions.clickElement("commentPage.likeButton");

    }
    public void deleteComment(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.waitForElementPresent("commentPage.showCommentsBtn");
        actions.scrollUntilElementIsDisplayed("commentPage.showCommentsBtn");
        actions.clickElement("commentPage.showCommentsBtn");
        actions.waitForElementPresent("commentPage.deleteComment");
        actions.clickElement("commentPage.deleteComment");
        actions.waitForElementPresent("commentPage.deleteCommentTitle");
        actions.scrollDown();
        actions.selectFromDropDownMenu("true","commentPage.dropDownMenuToDelete");
        actions.clickElement("commentPage.submitDeleteBtn");
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.clickElement("homePage.homeButton");
    }
}
