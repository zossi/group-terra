package pages;


import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import utils.Utils;

public class ConnectionPage extends BasePage{
    private static final String EXPECTEDMESSAGEFORSUCCESSFULLCONNECTIONREQUEST = "Good job! You have send friend request!";

    public ConnectionPage(WebDriver driver) {
        super(driver, "base.url");
    }

    public void loginAdmin(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.clickElement("homePage.signInButton");
        actions.typeValueInField(Utils.getConfigPropertyByKey("credentials.adminUsername"), "loginPage.usernameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("credentials.defaultPassword"), "loginPage.passwordField");
        actions.clickElement("loginPage.logInButton");
    }



    public void navigateToUserProfile(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.clickElement("homePage.searchButton");
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.waitForElementPresent("connectionPage.seeProfileButton");
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.moveToElement("connectionPage.seeProfileButton");
        actions.waitForInvisibilityOfElementLocated("navBarCollapseCover");
        actions.clickElement("connectionPage.seeProfileButton");
    }

    public void sendConnectionRequestToUser(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.waitForInvisibilityOfElementLocated("navBarCollapseCover");
        actions.clickElement("connectionPage.connectButton");
        actions.scrollUpJS();
    }

    public void assertMessageConnectionRequestSent() {
        Assert.assertEquals(EXPECTEDMESSAGEFORSUCCESSFULLCONNECTIONREQUEST, actions.getTextOfElement("connectionPage.requestSuccessfulMessage"));
    }
}
