package pages;


import org.openqa.selenium.WebDriver;
import utils.Utils;

public class AdminPage extends BasePage{


    public AdminPage(WebDriver driver) {
        super(driver, "base.url");
    }


     public void goToAdminZone(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.clickElement("adminPage.goToAdminBtn");
    }

    public void displayAllUsers(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.clickElement("adminPage.viewUsersBtn");
    }

    public void navigateToUserProfile(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.scrollDown();
        actions.waitForElementPresent("adminPage.seeProfileBtn");
        actions.moveToElement("adminPage.seeProfileBtn");
        actions.clickElement("adminPage.seeProfileBtn");
    }

    public void disableUserProfile(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.scrollDown();
        actions.moveToElement("adminPage.disableBtn");
        actions.clickElement("adminPage.disableBtn");

    }
    public void enableUserProfile(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.scrollDown();
        actions.waitForElementPresent("adminPage.enableBtn");
        actions.moveToElement("adminPage.enableBtn");
        actions.clickElement("adminPage.enableBtn");
    }

    public void sendFriendRequestToUser(){
        actions.scrollDownJS();
        actions.scrollDownJS();
        actions.scrollDownJS();
        actions.scrollDownJS();
        actions.scrollDownJS();
        actions.waitForElementPresent("commentPage.showCommentsBtn");
        actions.moveToElement("commentPage.showCommentsBtn");
        actions.clickElement("commentPage.showCommentsBtn");
        actions.waitForElementPresent("commentPage.deleteComment");
        actions.clickElement("commentPage.deleteComment");

    }
}
