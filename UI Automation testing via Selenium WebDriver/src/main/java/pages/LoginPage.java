package pages;

import org.openqa.selenium.WebDriver;
import utils.Utils;

public class LoginPage extends BasePage {
    private static final String EXPECTEDMESSAGENOTVALIDCREDENTIALS = "Wrong username or password.";
    private static final String EXPECTEDMESSAGEFORSUCCESSFULLOGOUT = "You have been logged out.";


    public LoginPage(WebDriver driver) {
        super(driver, "base.url");
    }

    public void navigateToLoginPage() {
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.waitForElementPresent("homePage.signInButton");
        actions.clickElement("homePage.signInButton");
    }

    public void goBackToHomePage() {
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.scrollUpJS();
        actions.clickElement("homePage.logoButton");
    }

    public void loginUserWithValidCredentials(String key) {
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.scrollDown();
        actions.typeValueInField(Utils.getConfigPropertyByKey(key), "loginPage.usernameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("credentials.defaultPassword"), "loginPage.passwordField");
        actions.clickElement("loginPage.logInButton");
    }

    public void loginUserWithoutAnyData() {
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.waitForElementPresent("loginPage.logInButton");
        actions.clickElement("loginPage.logInButton");
    }

    public void logoutUser() {
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.clickElement("loginPage.logoutButton");
    }

    public void assertSuccessfulLogin() {
        actions.assertElementPresent("updateProfilePage.personalProfileButton");
    }

    public void assertMessageNotValidCredentials() {
        actions.assertMessagePresent(EXPECTEDMESSAGENOTVALIDCREDENTIALS, "loginPage.errorMessage");
    }

    public void assertMessageUserLoggedOut() {
        actions.assertMessagePresent(EXPECTEDMESSAGEFORSUCCESSFULLOGOUT, "loginPage.successfulLogoutMessage");
    }
}
