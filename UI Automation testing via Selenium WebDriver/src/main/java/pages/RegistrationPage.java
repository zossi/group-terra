package pages;

import org.openqa.selenium.WebDriver;
import utils.Utils;

public class RegistrationPage extends BasePage {
    private static final String EXPECTEDMESSAGNOTVALIDEMAIL = "this doesn't look like valid email";
    private static final String EXPECTEDMESSAGEFORSUCCESSREGISTRATION = "Welcome to our community.";
    private static final String EXPECTEDALERTMESSAGE ="Please";
    private static final String EXPECTEDMESSAGEFORSHORTPASSWORD="password must be minimum 6 characters";
    private static final int START_VALUE_TO_SELECT_PROFESSION = 100;
    private static final int END_VALUE_TO_SELECT_PROFESSION = 157;
    private static final int ONE_WORD_TO_TYPE = 1;
    private static final int NUMBER_ONE = 1;
    private static final int NUMBER_TEN = 10;


    public RegistrationPage(WebDriver driver) {
        super(driver, "base.url");
    }


    public  void navigateToRegisterPage(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");

        actions.waitForElementPresent("homePage.registerFrontPageButton");
        actions.clickElement("homePage.registerFrontPageButton");
    }
    public void goBackToHomePage(){
         actions.waitForInvisibilityOfElementLocated("fullscreenCover");
         actions.scrollUpJS();
         actions.clickElement("homePage.logoButton");
    }
    public void registerNewUser() {

        actions.waitForElementPresent("registerPage.passwordField");
        actions.typeValueInField(actions.getLorem().getWords(1), "registerPage.nameField");
        actions.typeValueInField(actions.getLorem().getEmail(), "registerPage.emailField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("credentials.defaultPassword"), "registerPage.passwordField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("credentials.defaultPassword"), "registerPage.confirmPasswordField");
        actions.scrollDown();
        actions.waitForElementPresent("registerPage.registerButton");
        actions.selectFromDropDownMenu(Integer.toString(actions.getRandomNumber(START_VALUE_TO_SELECT_PROFESSION, END_VALUE_TO_SELECT_PROFESSION)),"registerPage.selectProfession");
        actions.clickElement("registerPage.registerButton");
    }

    public void registerNewUserWithoutAnyData() {

        actions.waitForElementPresent("registerPage.registerButton");
        actions.clickElement("registerPage.registerButton");
    }

    public void registerNewUserWithInvalidEmail() {
        String invalidEmail = actions.getLorem().getWords(ONE_WORD_TO_TYPE) +".gmail.com";
        actions.waitForElementPresent("registerPage.passwordField");
        actions.typeValueInField(actions.getLorem().getWords(ONE_WORD_TO_TYPE), "registerPage.nameField");
        actions.typeValueInField(invalidEmail, "registerPage.emailField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("credentials.defaultPassword"),  "registerPage.passwordField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("credentials.defaultPassword"),  "registerPage.confirmPasswordField");
        actions.scrollDown();
        actions.waitForElementPresent("registerPage.registerButton");
        actions.selectFromDropDownMenu(Integer.toString(actions.getRandomNumber(START_VALUE_TO_SELECT_PROFESSION,END_VALUE_TO_SELECT_PROFESSION)),"registerPage.selectProfession");
        actions.clickElement("registerPage.registerButton");
    }

    public void registerNewUserWithShortPassword() {

        String shortPassword = "ab"+actions.getRandomNumber(NUMBER_ONE, NUMBER_TEN);
        actions.waitForElementPresent("registerPage.passwordField");
        actions.typeValueInField(actions.getLorem().getWords(ONE_WORD_TO_TYPE), "registerPage.nameField");
        actions.typeValueInField(actions.getLorem().getEmail(), "registerPage.emailField");
        actions.typeValueInField(shortPassword, "registerPage.passwordField");
        actions.typeValueInField(shortPassword, "registerPage.confirmPasswordField");
        actions.selectFromDropDownMenu( Integer.toString(actions.getRandomNumber(START_VALUE_TO_SELECT_PROFESSION,END_VALUE_TO_SELECT_PROFESSION)),"registerPage.selectProfession");
        actions.clickElement("registerPage.registerButton");
    }
    public void assertSuccessfulRegistration() {
        actions.assertMessagePresent(EXPECTEDMESSAGEFORSUCCESSREGISTRATION, "registerPage.greetingMessageToSuccessRegistration");
    }
    public void assertMessageNotValidEmail(){
       actions.assertMessagePresent(EXPECTEDMESSAGNOTVALIDEMAIL,"registerPage.errorMessageForNotValidEmail");
    }

    public void assertMessageShortPassword(){
        actions.assertMessagePresent(EXPECTEDMESSAGEFORSHORTPASSWORD,"registerPage.errorMessageForShortPassword");
    }
    public void assertPopUpAlertMessageForRegisterFields() {
        actions.assertPopUpAlertMessage(EXPECTEDALERTMESSAGE);
    }
}
