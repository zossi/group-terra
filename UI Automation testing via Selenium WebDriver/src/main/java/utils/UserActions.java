package utils;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import java.util.Random;


public class UserActions {
    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;
    final Actions actions;

    public UserActions() {
        this.actions = new Actions(Utils.getWebDriver());
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

 //=================================Element and Action====================================
    public void clickElement(String key, Object... arguments) {
        String locator = getLocatorValueByKey(key, arguments);

        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(locator));
        element.click();
    }
    public void clickElementJS(String key, Object... arguments) {
        String locator = getLocatorValueByKey(key, arguments);
        WebElement element = driver.findElement(By.xpath(locator));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", element);
    }
    public void scrollDownJS(){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,700)");
    }
    public void scrollUpJS(){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,-700)", "");
    }
    public void scrollUntilElementIsDisplayed(String key,Object... arguments ){
        String locator = getLocatorValueByKey(key,arguments);
        WebElement element = driver.findElement(By.xpath(locator));
        waitFor(2000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", element);
    }
    public void scrollDown(){
        WebElement body = driver.findElement(By.tagName("body"));

        Actions scrollDown = new Actions(driver)
                .moveToElement(body, - 10, 15) // position mouse over scrollbar
                .clickAndHold()
                .moveByOffset(0, 50) // scroll down
                .release();

        scrollDown.perform();
    }
    public void moveToElement(String key,Object... arguments ){
        String locator = getLocatorValueByKey(key,arguments);
        waitFor(3000);
        WebElement element = driver.findElement(By.xpath(locator));
        Actions actions = new Actions(driver);

        actions.moveToElement(element).build().perform();

    }
    public String getTextOfElement(String key, Object... arguments) {
        String locator = getLocatorValueByKey(key, arguments);
        WebElement element = driver.findElement(By.xpath(locator));
        return element.getText();
    }
    public void selectFromDropDownMenu(String text, String key, Object... arguments){
        String locator = getLocatorValueByKey(key, arguments);
        Select selectItem = new Select(driver.findElement(By.xpath(locator)));
        selectItem.selectByValue(text);
    }
    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = getLocatorValueByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }
    public int getSizeOfWebListOfElements(String key, Object... arguments) {
        String locator = getLocatorValueByKey(key, arguments);
        List<WebElement> element = driver.findElements(By.xpath(locator));
        return element.size();
    }

    public void clearTextField(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.clear();
    }
    public boolean isDisplayed(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        try {
            WebElement element = driver.findElement(By.xpath(locator));
            return  element.isDisplayed();
        }  catch(Exception e)
        {  return false;  }
    }

    public Lorem getLorem(){
        Lorem loremText = LoremIpsum.getInstance();
        return loremText;
    }

    public int getRandomNumber(int low, int high) {
        Random r = new Random();
        // String result = Integer.toString(randomNum);
        return r.nextInt(high - low) + low;
    }
    //############# WAITS #########

    public void waitForElementVisible(String locatorKey, Object... arguments) {
        Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));

        waitForElementVisibleUntilTimeout(locatorKey, defaultTimeout, arguments);
    }

    public void waitForElementVisibleUntilTimeout(String locator, int seconds, Object... locatorArguments) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        String xpath = getLocatorValueByKey(locator, locatorArguments);
        try {
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
        } catch (Exception exception) {
            Assert.fail("Element with locator: '" + xpath + "' was not found.");
        }
    }

    public void waitForElementPresent(String locator, Object... arguments) {
        WebDriverWait wait = new WebDriverWait(driver, 200);
        try {
            isElementPresent(locator);
        } catch (Exception exception) {
            Assert.fail("Element with locator: '" + locator + "' was not found.");
        }
    }
    public void waitForInvisibilityOfElementLocated(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
    }

    public boolean isElementPresent(String locator, Object... arguments) {
        try {
            Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
            WebDriverWait wait = new WebDriverWait(driver, defaultTimeout);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(Utils.getUIMappingByKey(locator))));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean isElementVisible(String locator, Object... arguments) {
        try {
            Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
            WebDriverWait wait = new WebDriverWait(driver, defaultTimeout);
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Utils.getUIMappingByKey(locator))));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public void waitFor(long timeOutMilliseconds) {
        try {
            Thread.sleep(timeOutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //############# ASSERTS #########

    public void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }
    public void assertMessagePresent(String expectedText, String key){
        String locator = Utils.getUIMappingByKey(key);
        WebElement msg=driver.findElement(By.xpath(locator));
        String text=msg.getText();
        Assert.assertEquals(expectedText, text);
    }
    public void assertPopUpAlertMessage(String expectedText){
        try {
            WebDriverWait wait = new WebDriverWait(driver, 2);
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            alert.accept();
            Assert.assertTrue(alert.getText().contains(expectedText));
        } catch (Exception e) {
           System.out.print("No pop-up alert");
        }

    }
    public void assertElementAttribute(String locator, String attributeName, String attributeValue) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));

        Assert.assertEquals(element.getAttribute(attributeName), attributeValue);
    }

    public void assertNavigatedUrl(String urlKey) {
        String currentUrl = driver.getCurrentUrl();
        String expectedUrl = Utils.getConfigPropertyByKey(urlKey);
        Assert.assertTrue( "Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + expectedUrl, currentUrl.contains(expectedUrl));
    }

    public void pressKey(Keys key) {
        Actions action = new Actions(driver);
        action.keyDown(key).keyUp(key);
    }

    private String getLocatorValueByKey(String locator, Object[] arguments) {
        return String.format(Utils.getUIMappingByKey(locator), arguments);
    }

}
