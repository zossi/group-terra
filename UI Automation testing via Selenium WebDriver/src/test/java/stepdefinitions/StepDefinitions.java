package stepdefinitions;

import utils.UserActions;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class StepDefinitions extends BaseStepDefinitions{
    UserActions actions = new UserActions();

    @Given("Anonymous user types $value in $field profession field")
    public void typeInField(String value, String field){
        actions.typeValueInField(value, field);
    }

    @When("Clicks on $element button")
    public void clickElement(String element){actions.clickElement(element);}

    @Then("All users with profession Baker are displayed and have $field see personal profile button")
    public void assertProfileDisplayed( String field){
        actions.assertElementPresent( field);
    }
}




