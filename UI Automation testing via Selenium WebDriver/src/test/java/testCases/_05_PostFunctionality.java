package testCases;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.testng.Assert;
import pages.LoginPage;
import pages.PostPage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class _05_PostFunctionality extends BaseTest {
    PostPage postPage = new PostPage(actions.getDriver());
    LoginPage logInActions = new LoginPage(actions.getDriver());
    private int numberPostsBefore = actions.getSizeOfWebListOfElements("postPage.listOfAllPosts");
    private int numberPostsAfter = actions.getSizeOfWebListOfElements("postPage.listOfAllPosts");

    @Before
    public void logInUser() {
        logInActions.navigateToLoginPage();
        logInActions.loginUserWithValidCredentials("credentials.defaultUsername");
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.moveToElement("loginPage.logoutButton");
        actions.assertElementPresent("loginPage.logoutButton");
    }

    @After
    public void logOutUser() {
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.clickElement("homePage.homeButton");
        logInActions.logoutUser();
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        actions.moveToElement("homePage.signInButton");
        actions.assertElementPresent("homePage.signInButton");
    }

    @Test
    public void TC05_001_createNewPublicPostWithShortMessage() {
        postPage.loadAllPosts();
        numberPostsBefore = actions.getSizeOfWebListOfElements("postPage.listOfAllPosts");
        actions.assertElementPresent("postPage.newPostButton2");

        postPage.createPublicPostWithShortMessage();
        numberPostsAfter = actions.getSizeOfWebListOfElements("postPage.listOfAllPosts");
        Assert.assertNotEquals(numberPostsBefore, numberPostsAfter);
    }

    @Test
    public void TC05_004_createNewPrivatePostWithLongMessage() {
        postPage.loadAllPosts();
        numberPostsBefore = actions.getSizeOfWebListOfElements("postPage.listOfAllPosts");
        actions.assertElementPresent("postPage.newPostButton2");
        postPage.createPrivatePostWithLongMessage();
        numberPostsAfter = actions.getSizeOfWebListOfElements("postPage.listOfAllPosts");
        Assert.assertNotEquals(numberPostsBefore, numberPostsAfter);
    }



    @Test
    public void TC05_010_likePost() {
        postPage.loadAllPosts();
        actions.scrollDown();
        postPage.likePost();
        actions.assertElementPresent("postPage.dislikeButton");
    }

    @Test
    public void TC05_012_deletePost() {
        postPage.loadAllPosts();
        actions.scrollDown();
        numberPostsBefore = actions.getSizeOfWebListOfElements("postPage.listOfAllPosts");
        postPage.deletePost();
        numberPostsAfter = actions.getSizeOfWebListOfElements("postPage.listOfAllPosts");
        Assert.assertNotEquals(numberPostsBefore, numberPostsAfter);
    }

}
