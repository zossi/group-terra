package testCases;


import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import pages.AnonymousUserPage;
import utils.Utils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class _01_AnonymousUser extends BaseTest{
    AnonymousUserPage anonymousUser = new AnonymousUserPage(actions.getDriver());

    @Test
    public void TC01_001_searchByProfession(){
       actions.waitForElementPresent("homePage.seeProfileButton");
       anonymousUser.searchAndSubmitByProfession(Utils.getConfigPropertyByKey("credentials.defaultProfession"));
       anonymousUser.assertResultOfSearchByProfession();

    }
    @Test
    public void TC01_002_searchByNoneProfessionAndName(){
        actions.waitFor(10);
        anonymousUser.searchAndSubmitByNoneFillInFields();
        anonymousUser.assertPopUpAlertMessageForSearchFields();

    }
    @Test
    public void TC01_004_searchByProfessionAndNameOfRecommendedPublicProfile(){
        actions.waitForElementPresent("homePage.seeProfileButton");
        anonymousUser.searchAndSubmitByProfessionAndName(Utils.getConfigPropertyByKey("credentials.defaultProfession"),Utils.getConfigPropertyByKey("credentials.fullName"));
        anonymousUser.assertResultOfSearchByProfession();
    }
    @Test
    public void TC01_008_navigateToAboutUsPage() {
        actions.waitForElementPresent("homePage.aboutUsPageTitle");
        anonymousUser.navigateToAboutUsPage();
        actions.assertElementPresent("homePage.aboutUsPageTitle");
    }
    @Test
    public void TC01_009_logInAsAnonymousUser(){
        actions.waitForElementPresent("loginPage.logInButton");
        anonymousUser.loginWhenUserIsUnregistered();
        anonymousUser.assertLoginMessageNotRegisteredUser();

    }

}
