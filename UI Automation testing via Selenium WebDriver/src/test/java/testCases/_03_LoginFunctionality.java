package testCases;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import pages.LoginPage;
import pages.RegistrationPage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class _03_LoginFunctionality extends BaseTest{

    LoginPage logInPage = new LoginPage(actions.getDriver());

 @Before
    public void navigateToLoginPage(){
        logInPage.navigateToLoginPage();
        actions.assertElementPresent("loginPage.usernameField");
    }

   @Test
    public void TC03_001_loginUserWithValidCredentials(){
       logInPage.loginUserWithValidCredentials("credentials.defaultUsername");
       logInPage.assertSuccessfulLogin();
       actions.assertElementPresent("loginPage.logoutButton");
       logInPage.logoutUser();
       actions.waitForElementPresent("homePage.logoButton");
       logInPage.goBackToHomePage();
       actions.assertElementPresent("homePage.signInButton");
    }

    @Test
    public void TC03_002_loginUserWithoutAnyCredentials(){
        logInPage.loginUserWithoutAnyData();
        logInPage.assertMessageNotValidCredentials();
        logInPage.goBackToHomePage();
        actions.assertElementPresent("homePage.signInButton");
    }

    @Test
    public void TC03_010_logoutUser() {
        logInPage.loginUserWithValidCredentials("credentials.defaultUsername");
        actions.assertElementPresent("loginPage.logoutButton");
        logInPage.logoutUser();
        logInPage.assertMessageUserLoggedOut();
        logInPage.goBackToHomePage();
        actions.assertElementPresent("homePage.signInButton");
    }

}
