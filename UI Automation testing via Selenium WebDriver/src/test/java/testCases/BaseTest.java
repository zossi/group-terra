package testCases;


import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.interactions.Actions;
import pages.RegistrationPage;
import utils.UserActions;

public class BaseTest {
	UserActions actions = new UserActions();
	Actions act = new Actions(actions.getDriver());


	@BeforeClass
	public static void setUp(){
		UserActions.loadBrowser("base.url");

	}

	@AfterClass
	public static void tearDown(){
		UserActions.quitDriver();
	}
}
