package testCases;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import pages.RegistrationPage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class _02_RegisterFunctionality extends BaseTest {

    RegistrationPage registerPage = new RegistrationPage(actions.getDriver());

    @Before
    public void navigateToRegisterPage() {
        registerPage.navigateToRegisterPage();
        actions.assertElementPresent("registerPage.nameField");
    }

    @Test
    public void TC02_001_registerNewUserWithValidCredentials() {
        registerPage.registerNewUser();
        actions.assertElementPresent("registerPage.updateYourProfileButton");
        actions.waitForElementPresent("homePage.logoButton");
        registerPage.goBackToHomePage();
        actions.assertElementPresent("homePage.registerFrontPageButton");

    }

    @Test
    public void TC02_002_registerNewUserWithoutAnyCredentials() {
        registerPage.registerNewUserWithoutAnyData();
        registerPage.assertPopUpAlertMessageForRegisterFields();
        registerPage.goBackToHomePage();
        actions.assertElementPresent("homePage.registerFrontPageButton");
    }

    @Test
    public void TC02_004_registerNewUserWithInvalidEmail() {
        registerPage.registerNewUserWithInvalidEmail();
        registerPage.assertMessageNotValidEmail();
        registerPage.goBackToHomePage();
        actions.assertElementPresent("homePage.registerFrontPageButton");
    }

    @Test
    public void TC02_008_registerNewUserWithShortPassword() {
        registerPage.registerNewUserWithShortPassword();
        registerPage.assertMessageShortPassword();
        registerPage.goBackToHomePage();
        actions.assertElementPresent("homePage.registerFrontPageButton");
    }

}
