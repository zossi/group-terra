package testCases;


import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import pages.AdminPage;
import pages.LoginPage;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class _08_AdminFunctionality extends BaseTest{
    AdminPage adminPage = new AdminPage(actions.getDriver());
    LoginPage logInActions=new LoginPage(actions.getDriver());

  @Before
   public void logInUserAndNavigateToAllUsersAtAdminZone() {
      logInActions.navigateToLoginPage();
      logInActions.loginUserWithValidCredentials("credentials.adminUsername");
      actions.moveToElement("loginPage.logoutButton");
      actions.assertElementPresent("loginPage.logoutButton");
      actions.waitForInvisibilityOfElementLocated("fullscreenCover");
      adminPage.goToAdminZone();
      actions.moveToElement("adminPage.viewUsersBtn");
      actions.assertElementPresent("adminPage.viewUsersBtn");
      adminPage.displayAllUsers();
      actions.moveToElement("adminPage.seeProfileBtn");
      actions.assertElementPresent("adminPage.seeProfileBtn");
 }
 @After
 public void logOutUser(){
     logInActions.logoutUser();
     actions.moveToElement("homePage.logoButton");
     actions.assertElementPresent("homePage.logoButton");
 }
    @Test
    public void TC08_027_disableUserProfile(){

        adminPage.navigateToUserProfile();
        actions.moveToElement("adminPage.userProfilePic");
        actions.assertElementPresent("adminPage.userProfilePic");
        adminPage.disableUserProfile();
        actions.assertElementPresent("adminPage.enableBtn");
    }

    @Test
    public void TC08_028_enableUserProfile(){
        adminPage.navigateToUserProfile();
        actions.moveToElement("adminPage.userProfilePic");
        actions.assertElementPresent("adminPage.userProfilePic");
        adminPage.enableUserProfile();
        actions.assertElementPresent("adminPage.disableBtn");
    }


}
