package testCases;

import org.junit.After;
import org.junit.Before;

import org.junit.Test;
import org.testng.Assert;
import pages.LoginPage;
import pages.UpdateUserProfilePage;

public class _04_UpdateUserProfile extends BaseTest{
   UpdateUserProfilePage profilePage = new UpdateUserProfilePage(actions.getDriver());
   LoginPage logInPage = new LoginPage(actions.getDriver());
   @Before
   public void loginUser(){
       logInPage.navigateToLoginPage();
       logInPage.loginUserWithValidCredentials("credentials.defaultUsername");
       actions.assertElementPresent("loginPage.logoutButton");
   }
   @After
   public void logOut(){
       logInPage.logoutUser();
       actions.assertElementPresent("homePage.signInButton");
   }
   @Test
    public void TC04_001_updatePersonalProfile(){
          profilePage.navigateToPersonalProfilePage();
          actions.assertElementPresent("updateProfilePage.editPersonalInfoButton");
          profilePage.updateUserPersonalInfo();
          actions.scrollDown();
          Assert.assertNotEquals(profilePage.fullNameBefore, profilePage.fullNameAfter);
   }
    @Test
    public void TC04_009_updateUserProfession(){
        profilePage.navigateToPersonalProfilePage();
        actions.assertElementPresent("updateProfilePage.editPersonalInfoButton");
        profilePage.updateProfessionalCategory();
        Assert.assertNotEquals(profilePage.professionBefore, profilePage.professionAfter);
    }
    @Test
    public void TC04_010_updateUserProvidedServicesAndAvailability(){
        profilePage.navigateToPersonalProfilePage();
        actions.assertElementPresent("updateProfilePage.editPersonalInfoButton");
        profilePage.updateSkillsAndHours();
        Assert.assertNotEquals(profilePage.hoursAvailabilityBefore, profilePage.hoursAvailabilityAfter);
    }


}
