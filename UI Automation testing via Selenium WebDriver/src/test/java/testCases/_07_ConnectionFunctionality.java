package testCases;


import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import pages.ConnectionPage;
import pages.LoginPage;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class _07_ConnectionFunctionality extends BaseTest{
    ConnectionPage connectionPage = new ConnectionPage(actions.getDriver());
    LoginPage logInActions= new LoginPage(actions.getDriver());

  @Before
   public void logIn() {
      logInActions.navigateToLoginPage();
      logInActions.loginUserWithValidCredentials("credentials.defaultUsername");
      actions.waitForElementPresent("loginPage.logoutButton");
      actions.moveToElement("loginPage.logoutButton");
      actions.assertElementPresent("loginPage.logoutButton");

 }
 @After
 public void logOutUser(){
     logInActions.logoutUser();
     actions.moveToElement("homePage.signInButton");
     actions.assertElementPresent("homePage.signInButton");
 }

    @Test
    public void TC07_001_connectWithUser(){
        actions.waitForInvisibilityOfElementLocated("fullscreenCover");
        connectionPage.navigateToUserProfile();
        connectionPage.sendConnectionRequestToUser();
        connectionPage.assertMessageConnectionRequestSent();
    }


}
