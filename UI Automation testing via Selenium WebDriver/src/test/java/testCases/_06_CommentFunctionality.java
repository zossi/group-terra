package testCases;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.testng.Assert;
import pages.CommentPage;
import pages.LoginPage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class _06_CommentFunctionality extends BaseTest{
    CommentPage commentPage = new CommentPage(actions.getDriver());
    LoginPage logInActions= new LoginPage(actions.getDriver());
  @Before
   public void logInUser() {
      logInActions.navigateToLoginPage();
      logInActions.loginUserWithValidCredentials("credentials.defaultUsername");
      actions.waitForInvisibilityOfElementLocated("fullscreenCover");
      actions.moveToElement("loginPage.logoutButton");
      actions.assertElementPresent("loginPage.logoutButton");
 }
  @After
   public void logOutUser(){
     logInActions.logoutUser();
     actions.waitForInvisibilityOfElementLocated("fullscreenCover");
     actions.moveToElement("homePage.signInButton");
     actions.assertElementPresent("homePage.logoButton");
 }
    @Test
    public void TC06_001_createNewComment(){
        commentPage.loadAllPosts();
        actions.scrollDown();
        actions.assertElementPresent("commentPage.exploreUserPostBtn");
        commentPage.navigateToUserProfile();
        actions.assertElementPresent("commentPage.allPostsOfThisUserBtn");
        actions.scrollDownJS();
        actions.scrollDownJS();
        actions.scrollDownJS();
        int numberCommentsBefore= actions.getSizeOfWebListOfElements("commentPage.listOfAllComment");
        commentPage.createComment();
        int numberCommentsAfter= actions.getSizeOfWebListOfElements("commentPage.listOfAllComment");
        Assert.assertNotEquals(numberCommentsBefore,numberCommentsAfter);
    }

    @Test
    public void TC06_005_likeComment(){

        commentPage.loadAllPosts();
        actions.scrollDown();
        actions.moveToElement("commentPage.exploreUserPostBtn");
        actions.assertElementPresent("commentPage.exploreUserPostBtn");
        commentPage.navigateToUserProfile();
        actions.moveToElement("commentPage.allPostsOfThisUserBtn");
        actions.assertElementPresent("commentPage.allPostsOfThisUserBtn");
        commentPage.likeComment();
        actions.assertElementPresent("commentPage.dislikeButton");
    }

    @Test
    public void TC06_007_deleteComment() {

        commentPage.loadAllPosts();
        actions.scrollDown();
        actions.moveToElement("commentPage.exploreUserPostBtn");
        actions.assertElementPresent("commentPage.exploreUserPostBtn");
        commentPage.navigateToUserProfile();
        actions.moveToElement("commentPage.allPostsOfThisUserBtn");
        actions.assertElementPresent("commentPage.allPostsOfThisUserBtn");
        int numberCommentsBefore= actions.getSizeOfWebListOfElements("commentPage.listOfAllComment");
        commentPage.deleteComment();
        int numberCommentsAfter= actions.getSizeOfWebListOfElements("commentPage.listOfAllComment");
        Assert.assertNotEquals(numberCommentsBefore,numberCommentsAfter);
    }

}
