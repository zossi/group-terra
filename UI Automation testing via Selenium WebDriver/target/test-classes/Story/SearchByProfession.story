Meta:
@endToEnd

Narrative:
As an anonymous user of WEare Social NetWork app
I would like to search for public users' profiles by a specific profession

Scenario: End to end scenario in social network
Given Anonymous user types Baker in homePage.searchInputFieldProfession profession field
When Clicks on homePage.searchButton button
Then All users with profession Baker are displayed and have homePage.seeProfileButton see personal profile button
