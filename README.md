# Alpha 28 QA - Team Terra's Final Project - WEare Social Network

</br>

### Greetings fellow QA enthusiasts, we are Antoniya, Bozhidara-Michelle and Nadezhda from **Team Terra**.

</br>

```sh
Below you may find links to additional files, resources and supporting information, relevant to this project.
```

</br>

**Antoniya Petrova** 
[_GitLab Personal Repository Link_](https://gitlab.com/Tony_Petrova/antoniya_petrova_repo.git)

**Bozhidara-Michelle Vicheva**
[_GitLab Personal Repository Link_](https://gitlab.com/bozhidara-michelle/telerik-academy-practice-bozhidara-michelle-vicheva)

**Nadezhda Glushkova**
[_GitLab Personal Repository Link_](https://gitlab.com/zossi/nadia-glushkova-repo)

</br>

| **Resource**                       | **Location**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0. **Team Trello**                 | [_Trello Workspace Link_](https://trello.com/invite/b/xsZh7M04/5f06277e8abe75c3f54add95a785f8e7/a28-qa-final-project)                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| 1. **Test Plan**                   | [_Test Plan Link_](https://telerikacademy-my.sharepoint.com/:b:/p/bozhidara-michelle_vicheva_a28_learn/Ea-zi0-xdXxJggtHNFyIgSIBHS0oKdE_xJnebRA15j-s_A?e=0J3nud) |
| 2. **Exploratory Testing**         | [_Testing Sessions and Report Link_](https://telerikacademy-my.sharepoint.com/:w:/p/bozhidara-michelle_vicheva_a28_learn/Ecpi10J1xSFPgofsW7dt5DMBFdefkLohnlwDde-tp0C2Mg?e=SPIDYb)                                                                                                                                                                                                                         |
| 3. **Template for Issues**         | [_Logging of Issues Template Link_](https://telerikacademy-my.sharepoint.com/:t:/p/bozhidara-michelle_vicheva_a28_learn/ERrcms9oH-NNmVQFNF74FW8BFTVtBgdSJd-doOvPP36LvQ)                          |
| 4. **Template for Test Cases**     | [_Test Cases Template Link_](https://telerikacademy-my.sharepoint.com/:t:/p/antoniya_petrova_a28_learn/EcyfRjETlBRKl1Y1DUSIniYBO7hwugnG3Hf-5k_yzhmdgA?e=TP5xAK)   |     
| 5. **Bug issues managed in GitLab**  | [_Bug issues Overview Link_](https://gitlab.com/zossi/group-terra/-/issues)|     
| 6. **Test Cases managed in Excel** | [_Test Cases Overview Link_](https://telerikacademy-my.sharepoint.com/:x:/p/antoniya_petrova_a28_learn/EdXBGLEy5zFNungeN3-UOF4B4uTpaL-6xU3YeUgN7BW4JA?e=8EpfWc)                   |                                                                                                                       
| 7. **Tests Summary Report**            | [_Tests Summary Report Link_](https://telerikacademy-my.sharepoint.com/:b:/p/bozhidara-michelle_vicheva_a28_learn/EQjldciGPApCkKHS1iVZjh0Bfa28GVkOeGEhtMaFW0hzxQ?e=5tx0PT)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| 8. **Final Presentation**                | [_Final Presentation Link_](https://telerikacademy-my.sharepoint.com/:p:/p/antoniya_petrova_a28_learn/EUYOi9kN8fFNtvaWwF-vytIB2kBGPN8dugs2GwmzCzH51Q?e=WG12to)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
