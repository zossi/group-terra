>>>># **REQUIRED PREREQUISITES TO RUN THE PROJECT AND TESTS**

##### *Alpha 28 QA - Team Terra's Final Project - WEare Social Network*
</br>

#### **STEP BY STEP TO RUN UI TESTS**

### 1. User should have installed Windows 10/Mac/Linux on his computer

### 2. Install Mozilla FireFox 

[Mozilla FireFox] (https://www.mozilla.org/en-US/firefox/new/) - click to link. The Download link in the green box will automatically detect your operating system and language.
If you want to download Firefox in another language, or for a different operating system, click the Systems & Language link right below the Download button.

### 3. Install Docker 
- [Windows](https://docs.docker.com/docker-for-windows/install/)
- [Mac](https://docs.docker.com/docker-for-mac/install/)
- [Linux](https://runnable.com/docker/install-docker-on-linux)
- Verify docker is present by starting terminal or cmd and enter `docker --version`
- If old instalation of docker is present - [update](https://docs.docker.com/docker-for-windows/install/#updates) it. 


### 4. Verify Docker Compose is present or install it (this step is needed for Linux OS)
 - [Install Docker Compose](https://docs.docker.com/compose/install/)

### 5. Clone the repository or download the file [docker-compose.yml](./docker-compose.yml)

### 6. Start terminal or cmd in the directory containing the `docker-compose.yml` file

### 7. Start/stop the application
- Run this command to start: `docker-compose up`
    - Images will be build/downloaded and start. If the terminal is closed - the app will stop!
- Run this command to start: `docker-compose up -d`
    - Images will be build/downloaded and started in containers in a detached mode (closing the terminal won't stop the application)
- Run this command to stop: `docker-compose down` 


### 8. Verify the app is running
- Open [http://localhost:8081/](http://localhost:8081/) in a browser

### 9. Install IntelliJ IDEA
[IntelliJ IDEA](https://www.jetbrains.com/idea/download/) -During the instalation of IntelliJ IDEA, user should select for "Create desctop shortcut" - 64-bit laucher

### 10. Install Maven on Windows
Maven is a build and dependency management tool for java based application development.
[Link](https://www.toolsqa.com/java/maven/how-to-install-maven-on-windows/) for setting up Maven tool step by step.

If your operation system is **Linux** [click here](https://www.decodingdevops.com/how-to-install-maven-in-linux-configure-maven-in-linux/) or **Mac** [click here](https://www.toolsqa.com/java/maven/how-to-install-maven-on-mac/), you should use links above. 

### 11. Install Git Bash locally 
[Git Bash](https://git-scm.com/downloads) - Git Bash is an application for Microsoft Windows environments that provides an emulation layer for a Git command-line experience.
Follow steps [here](https://www.stanleyulili.com/git/how-to-install-git-bash-on-windows/)

### 12. Clone the Final Project at a new folder on your computer 

- Click with right button on your mouse to open pop-up menu of your folder and click on Git Bash Here
- Type on comment line -> `git clone https://gitlab.com/zossi/group-terra.git`

### 13. Open //Localhost:8081 and register two default users:
 - First user:
    - username: admin 
    - email:admin@weare.com
    - password: aaa111
- Second user:
    - username: bunny 
    - email:bunny@gmail.com
    - password: aaa111
    
### 14. Run **.bat** files inside folder _WEareSocialNetwork_Project_

<br>
<br>
<br>

#### **STEP BY STEP TO RUN API TESTS**

### 1. User should have installed Windows 10/Mac/Linux on his computer

### 2. Install Docker 
- [Windows](https://docs.docker.com/docker-for-windows/install/)
- [Mac](https://docs.docker.com/docker-for-mac/install/)
- [Linux](https://runnable.com/docker/install-docker-on-linux)
- Verify docker is present by starting terminal or cmd and enter `docker --version`
- If old instalation of docker is present - [update](https://docs.docker.com/docker-for-windows/install/#updates) it. 

### 4. Verify Docker Compose is present or install it (this step is needed for Linux OS)
 - [Install Docker Compose](https://docs.docker.com/compose/install/)

### 5. Clone the repository or download the file [docker-compose.yml](./docker-compose.yml)

### 6. Start terminal or cmd in the directory containing the `docker-compose.yml` file

### 7. Start/stop the application
- Run this command to start: `docker-compose up`
    - Images will be build/downloaded and start. If the terminal is closed - the app will stop!
- Run this command to start: `docker-compose up -d`
    - Images will be build/downloaded and started in containers in a detached mode (closing the terminal won't stop the application)
- Run this command to stop: `docker-compose down` 

### 8. Verify the app is running
- Open [http://localhost:8081/](http://localhost:8081/) in a browser

### 9. Install **POSTMAN**
[Postman](https://www.jetbrains.com/idea/download/) 

## 10. Install Node.js locally
- [Node.js](https://nodejs.org/en/download/) Download *Windows Installer (.msi) 64-bit*
- Restart PC after installing Node.js
- Checking your version of npm and Node.js  `node -v` or `npm -v`

## 11. Install Newman
Newman is a command-line collection runner for Postman.
Open Command Prompt on your PC and type `npm install -g newman` and then `npm install -D jest-runner-newman`

## 12. Clone the Final Project at a new folder on your computer 

- Click with right button on your mouse to open pop-up menu of your folder and click on Git Bash Here
- Type on comment line -> **git clone https://gitlab.com/zossi/group-terra.git**

## 13. Run .bat file at folder POSTMAN


>>>>>>![enjoy](https://clipartart.com/images/thank-you-and-please-enjoy-clipart-3.png)
